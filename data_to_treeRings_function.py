import numpy as np
import matplotlib.pyplot as plt
import cv2

""" 
converts an image that has tree rings into an polar representation from which one can see if the choosen center of the TR is correct and the TR function as a function of the radius    
"""


path = "" #folder path
lmtd = np.load(path+"lmtd.npy") ## image in npy format, or any 2x2 matrix format [[],[]]
#resized = cv2.resize(lmtd,(IMG.shape[1],IMG.shape[0]), interpolation = cv2.INTER_AREA)
IMG = lmtd


xc,yc = 375, 395  # the center of the TR -- there is a seperate alhorithm 
                  #that finds this but it does not work well    
maxR = np.floor(np.sqrt(xc**2+yc**2))
image = IMG
margin = 1 
# radius resolution, angular resolution
# converts from polar to cart. the 3600 means the sample size per degree will be 3600/360 = 10 
polar_img = cv2.warpPolar(image, (int(maxR), 3600), (xc,yc), image.shape[0]*margin, cv2.WARP_POLAR_LINEAR)

polar_img = polar_img[1800:2700,:]
plt.ylabel("Angle [0.1 Degrees]")
plt.xlabel("Radius [px]")

## reference vertical lines
plt.vlines(490,0,900,alpha=0.5)
plt.vlines(390,0,900,alpha=0.5)
plt.vlines(330,0,900,alpha=0.5)
plt.imshow(polar_img),plt.show()

#%%
from scipy.signal.signaltools import wiener
imgplot = 0
#570,680
start = 190
cutout = polar_img[400:560,start:]
if imgplot:
    plt.ylabel("Angle [0.1 Degrees] with offset")
    plt.xlabel("Radius [px] with offset")
    plt.vlines(4900,0,900,alpha=0.5)
    plt.imshow(cutout),plt.show()

tr = cutout.mean(axis=0)
plt.plot(tr)



#%%

fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(10, 6))
axes[0].imshow(cutout)
axes[0].xaxis.tick_top()
axes[1].plot(wiener(tr)*100)
axes[1].set_ylabel("Change [%]")
axes[1].set_xlim(xmin=0,xmax=len(tr))
plt.tight_layout()
#%%

fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(10, 6))
axes.imshow(cutout)
axes.xaxis.tick_top()
AX=plt.gca()
AX.plot(-wiener(tr)*10000+55,'red',alpha=0.8)
AX.grid(False)
AX.set_xlim(xmin=0,xmax=len(tr))
plt.tight_layout()

#%%



