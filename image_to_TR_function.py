def get_spotgrid_image(xy=True,naned=False):
    from scipy.stats import binned_statistic, binned_statistic_2d
    import os
    
    path = "C:\\Users\\bahru\\Documents\\notebooks\\"
    os.chdir(path)
    
    x_arr = np.load('r02_s02_x_arr.npy')
    y_arr = np.load('r02_s02_y_arr.npy')
    xx_arr = np.load('r02_s02_xx_arr.npy')
    yy_arr = np.load('r02_s02_yy_arr.npy')
    xy_arr = np.load('r02_s02_xy_arr.npy')
    
    x = np.load(path+'r02_s02_x.npy')
    y = np.load(path+'r02_s02_y.npy')
    
    #averages all exposures together of each spot
    xx_med = np.nanmedian(xx_arr, axis=1)
    yy_med = np.nanmedian(yy_arr, axis=1)
    xy_med = np.nanmedian(xy_arr, axis=1)
    spot_filter = np.load('r02_s02_spotfilter.npy')
    xfltr = x_arr[spot_filter]
    yfltr = y_arr[spot_filter]
    xxfltr = xx_arr[spot_filter]
    yyfltr = yy_arr[spot_filter]
    xyfltr = xy_arr[spot_filter]
    
    xxmedfltr = xx_med[spot_filter]
    yymedfltr = yy_med[spot_filter]
    xymedfltr = xy_med[spot_filter]
    
    deltaXX = np.zeros(xxfltr.shape)
    deltaYY = np.zeros(yyfltr.shape)
    deltaXY = np.zeros(xyfltr.shape)
    
    for col in range(xxfltr.shape[1]):
        deltaXX[:,col] = xxfltr[:,col] - xxmedfltr
        deltaYY[:,col] = yyfltr[:,col] - yymedfltr
        deltaXY[:,col] = xyfltr[:,col] - xymedfltr
    
    deltaT = deltaXX + deltaYY
    nanmask = np.isfinite(xfltr) #masks all the NaN entries created when a catalog didn't have 2401 entries
    xfltr_flat = xfltr[nanmask].flatten()
    yfltr_flat = yfltr[nanmask].flatten()
    
    # dXX = deltaXX[nanmask].flatten()
    # dYY = deltaYY[nanmask].flatten()
    dXY = deltaXY[nanmask].flatten()
    dT = deltaT[nanmask].flatten()
    bins = [407,400] #approx. 10x10 px^2 binning
    if xy:
        dT = dXY
    dT_mean, x_edge, y_edge, binidx = binned_statistic_2d(xfltr_flat, yfltr_flat, dT, 'mean',
                                                          range=[[0,4072],[0,4000]], bins=bins)
                                                          #mean is significantly faster calculation
    
    MAX = 0.0088
    fcenter = [185,190]
    fradius = 140
    lmtd = np.where((dT_mean>MAX) | (dT_mean<-MAX),0,dT_mean)
    Mask = np.zeros(lmtd.shape)
    for i in range(lmtd.shape[0]):
        for j in range(lmtd.shape[1]):
            if (i-fcenter[0])**2 + (j-fcenter[1])**2 < fradius**2:
                Mask[i,j] = 1
            
    fig, axes = plt.subplots(figsize = (12,13))
    x, y = np.meshgrid(x_edge, y_edge)
    if naned:
        Mask = np.where(Mask==0,np.nan,Mask)
    lmtd = lmtd*Mask
    
    im0 = axes.pcolormesh(x, y, lmtd.T, vmin=-0.05, vmax=0.05)
    axes.set_xlabel('X position [pixels]', fontsize=15)
    axes.set_ylabel('Y position [pixels]', fontsize=15)
    axes.tick_params(axis='x', labelsize=12)
    axes.tick_params(axis='y', labelsize=12)
    axes.set_aspect('equal')
    
    # fig.subplots_adjust(bottom=0.2)
    # cbar_ax = fig.add_axes([0.15, 0.1, 0.7, 0.03])
    # cbar = fig.colorbar(im0, cax= cbar_ax, orientation='horizontal')
    # cbar.set_label(r'$\Delta$T [$\mathrm{pixels}^2$]', fontsize=18)
    # cbar.ax.tick_params(labelsize=12)
    
    return x, y, lmtd.T

X,Y,DT = get_spotgrid_image(1)    
#%%
fig, axes = plt.subplots(figsize = (12,13))
im0 = axes.pcolormesh(X,Y,DT, vmin=-0.013, vmax=0.02,alpha=0.69)
im1 = axes.imshow(IMG,origin='lower')
axes.set_xlabel('X position [pixels]', fontsize=15)
axes.set_ylabel('Y position [pixels]', fontsize=15)
axes.tick_params(axis='x', labelsize=12)
axes.tick_params(axis='y', labelsize=12)
axes.set_aspect('equal')

#%%
resized = cv2.resize(DT,(4072,4000), interpolation = cv2.INTER_AREA)

fig, axes = plt.subplots(figsize = (12,13))
im0 = axes.imshow(IMG,origin='lower')
im1 = axes.imshow(resized,origin='lower')
axes.set_xlabel('X position [pixels]', fontsize=15)
axes.set_ylabel('Y position [pixels]', fontsize=15)
axes.tick_params(axis='x', labelsize=12)
axes.tick_params(axis='y', labelsize=12)
axes.set_aspect('equal')

#%%

xc,yc = 4360, 4090
maxR = np.floor(np.sqrt(xc**2+yc**2))
image = resized
margin = 1 # Cut off the outer 10% of the image
# radius resolution, angular resolution
polar_img = cv2.warpPolar(image, (int(maxR), 3600), (xc,yc), image.shape[0]*margin, cv2.WARP_POLAR_LINEAR)
# Rotate it sideways to be more visually pleasing
polar_img = polar_img[1800:2700,:]
plt.ylabel("Angle [0.1 Degrees]")
plt.xlabel("Radius [px]")
plt.vlines(4900,0,900,alpha=0.5)
plt.imshow(polar_img),plt.show()

#%%
from scipy.signal.signaltools import wiener
imgplot = 1
#570,680
start = 2936
cutout = polar_img[336:490,start:]
if imgplot:
    plt.ylabel("Angle [0.1 Degrees] with offset")
    plt.xlabel("Radius [px] with offset")
    plt.vlines(4900,0,900,alpha=0.5)
    plt.imshow(cutout),plt.show()

tr = cutout.mean(axis=0)
plt.plot(tr)
plt.plot(wiener(tr,324))

filteredTR = wiener(tr- wiener(tr,224),4)
adjustedtrSG = np.concatenate((np.zeros(start),wiener(tr,10)))

#%%
fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(10, 6))
axes[0].imshow(cutout)
axes[0].xaxis.tick_top()
axes[1].plot(filteredTR)
axes[1].set_xlim(xmin=0,xmax=len(filteredTR))
plt.tight_layout()

#%%
fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(10, 6))
axes.imshow(cutout)
axes.xaxis.tick_top()
AX=plt.gca()
AX.plot(-filteredTR*30000+100,'red',alpha=0.4)
AX.grid(False)
AX.set_xlim(xmin=0,xmax=len(filteredTR))
plt.tight_layout()

#%%
ssst = 2900
plt.plot(adjustedtrSG[ssst:]*100)
plt.plot(-adjustedtr[ssst:]*600)
plt.annotate("Flat Field multiplied by -6",xy = (0,0.22))
plt.xlabel("Radius [px] + {} offset".format(ssst))
plt.ylabel("Devaition from mean [%]")
plt.legend(['Spotgrid','Flat Field'])
#%%
